# PREFIX is environment variable, but if it is not set, then set default value
ifeq ($(PREFIX),)
    PREFIX := /usr/local
endif

CC = gcc
CFLAGS = -fPIC -Wall -Wextra -O2
LDFLAGS = -shared -fPIC

SRCS = base.c helper.c irq.c mem.c attr.c
OBJS = $(SRCS:.c=.o)

.PHONY: all
all: libuio.so

libuio.so: $(OBJS)
	$(CC) $(LDFLAGS) -o $@ $^

$(SRCS:.c=.d):%.d:%.c
	$(CC) $(CFLAGS) -MM $< >$@

-include $(SRCS:.c=.d)

.PHONY: clean
clean:
	-${RM} ${TARGET_LIB} ${OBJS} $(SRCS:.c=.d)

.PHONY: install
install:
	install -d $(DESTDIR)$(PREFIX)/lib/
	install -m 644 libuio.so $(DESTDIR)$(PREFIX)/lib/
	install -d $(DESTDIR)$(PREFIX)/include/
	install -m 644 libuio.h $(DESTDIR)$(PREFIX)/include/
